package model;

import org.omg.PortableInterceptor.INACTIVE;

import java.sql.*;

/**
 * The type Database.
 */
public class Database {
    Connection connection;
    PreparedStatement preparedStatement;
    ResultSet resultSet;

    /**
     * Instantiates a new Database.
     */
    public Database(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/Project1","root",",./5mm1nu5l15");
            preparedStatement = connection.prepareStatement("select * from Observation");
            resultSet =preparedStatement.executeQuery();

            while (resultSet.next()){
                String veg_col = resultSet.getString(1);
                String veg_col1 = resultSet.getString(2);
                System.out.println(veg_col+"   "+veg_col1);
            }


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //create table Observation(name varchar(50) not null primary key, country varchar(30), year year, area_covered varchar(20))
    ///create table galamsey( galamsey_id int not null primary key auto_increment, observation varchar(50),veg_color varchar(10), color_value int(3),logitude varchar(20),latitude varchar(20),year year, foreign key(observation) references Observation(name));
//    create table Observation(
//    -> name varchar(50) not null primary key,
//    -> country varchar(30),
//    -> year year,
//    -> area_covered varchar(20));


    /**
     * Add observation.
     *
     * @param name              the name
     * @param country           the country
     * @param yearOfObservation the year of observation
     * @param areaCovered       the area covered
     * @throws SQLException the sql exception
     */
//Add parameters for the input for observation
    public void addObservation(String name, String country, Integer yearOfObservation, Double areaCovered) throws SQLException {

        preparedStatement = connection.prepareStatement("insert into Observation values " +
                "('"+name+"','"+country+"',"+yearOfObservation+","+areaCovered+")");
        preparedStatement.executeUpdate();
        System.out.println("Inserted");

    }

    /**
     * Add galamsey.
     *
     * @param Observation   the observation
     * @param veg_color     the veg color
     * @param col_value     the col value
     * @param logitude      the logitude
     * @param latitude      the latitude
     * @param year_of_event the year of event
     * @throws SQLException the sql exception
     */
    public void addGalamsey(String Observation, String veg_color, int col_value,Double logitude,Double latitude , Integer year_of_event) throws SQLException {

        preparedStatement = connection.prepareStatement(" insert into galamsey (observation,veg_color,color_value,logitude,latitude,year) " +
                "values ('"+Observation+"','"+veg_color+"','"+col_value+"','"+logitude+"','"+latitude+"',"+year_of_event+")");
        preparedStatement.executeUpdate();
        System.out.println("Inserted into Galamsey Successfully");

    }


}
